#include "tune/GridSearchPreprocessorTOML.hpp"

#include "tune/Error.hpp"

#include <algorithm>
#include <cassert>
#include <numeric>

namespace tune {

GridSearchPreprocessorTOML::GridSearchPreprocessorTOML(toml::table experiment)
        : experiment{std::move(experiment)}
{
    static constexpr std::size_t reserve_size = 8;
    grid_entries.reserve(reserve_size);
    extract_grid_entries(this->experiment);
}

const toml::table*
GridSearchPreprocessorTOML::generate_next_experiment() noexcept
{
    if (first) {
        first = false;
        return &experiment;
    }

    std::size_t i = 0;
    for (; i < grid_entries.size() &&
           grid_entries[i].values_index == grid_entries[i].values.size() - 1;
         ++i) {
        grid_entries[i].values_index = 0;
    }
    if (i == grid_entries.size()) { // No more experiments. Reset to beginning.
        for (auto& e : grid_entries) { e.values_index = 0; }
        first = true;
        return nullptr;
    }
    ++grid_entries[i].values_index;

    // Update the experiment table.
    for (const auto& e : grid_entries) {
        e.table_to_modify->erase(e.key_to_modify);
        e.table_to_modify->insert(e.key_to_modify, e.values[e.values_index]);
    }

    return &experiment;
}

toml::array GridSearchPreprocessorTOML::generate_all_experiments() noexcept
{
    std::size_t n_experiments = 1;
    for (const auto& entry : grid_entries) {
        n_experiments *= entry.values.size();
    }

    toml::array res;
    res.reserve(n_experiments);
    while (const auto* experiment = generate_next_experiment()) {
        res.push_back(*experiment);
    }
    return res;
}

std::vector<toml::table>
GridSearchPreprocessorTOML::preprocess(toml::array experiments)
{
    if (!experiments.is_array_of_tables()) {
        throw InvalidNodeError{{BasicTracePoint{"experiments"}}};
    }

    std::vector<toml::table> res;
    static constexpr std::size_t reserve_size = 32;
    res.reserve(reserve_size);
    for (const auto& experiment_node : experiments) {
        const auto& experiment = *experiment_node.as_table();
        GridSearchPreprocessorTOML preprocessor{experiment};
        while (const auto* e = preprocessor.generate_next_experiment()) {
            res.push_back(*e);
        }
    }
    return res;
}

void GridSearchPreprocessorTOML::extract_grid_entries(toml::table& table)
{
    if (auto* grid = table["grid"].as_table()) {
        for (auto& [key, values] : *grid) {
            if (table.contains(key)) {
                throw BasicAndGridKeyProvidedError{std::string{key.str()}};
            }
            auto* values_arr = values.as_array();
            if (!values_arr || !(values_arr->size() > 1)) {
                throw InvalidGridKeyError{std::string{key.str()}};
            }
            table.insert(key, (*values_arr)[0]);
            grid_entries.push_back(GridEntry{
                &table, std::string{key.str()}, std::move(*values_arr), 0});
        }
        table.erase("grid");
    }

    for (auto& [key, value] : table) {
        try {
            if (auto* tbl = value.as_table()) {
                extract_grid_entries(*tbl);
            } else if (auto* arr = value.as_array()) {
                extract_grid_entries(*arr);
            }
        } catch (const TraceError& exc) {
            exc.push_trace_point(BasicTracePoint{std::string{key.str()}});
            throw;
        }
    }
}

void GridSearchPreprocessorTOML::extract_grid_entries(toml::array& array)
{
    for (std::size_t i = 0; i < array.size(); ++i) {
        try {
            if (auto* tbl = array[i].as_table()) {
                extract_grid_entries(*tbl);
            } else if (auto* arr = array[i].as_array()) {
                extract_grid_entries(*arr);
            }
        } catch (const TraceError& exc) {
            exc.push_trace_point(BasicIndexTracePoint{i});
            throw;
        }
    }
}

}
