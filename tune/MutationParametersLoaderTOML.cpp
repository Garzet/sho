#include "tune/MutationParametersLoaderTOML.hpp"

#include "tune/Error.hpp"

namespace tune {

namespace floating_point {

std::unique_ptr<MutationParameters> load_gaussian_mutation_parameters(
    const ProblemParameters& /*problem_parameters*/,
    const toml::table& mutation)
{
    const auto std_dev = mutation["standard_deviation"].value<double>();
    if (!std_dev) {
        throw InvalidNodeError{{BasicTracePoint{"standard_deviation"}}};
    }

    return std::make_unique<GaussianMutationParameters>(*std_dev);
}

std::unique_ptr<MutationParameters>
load_random_mutation_parameters(const ProblemParameters& problem_parameters,
                                const toml::table& mutation)
{
    const auto min = mutation["range"][0].value<double>();
    if (!min) { throw InvalidNodeError{{IndexedTracePoint{"range", 0}}}; }
    const auto max = mutation["range"][1].value<double>();
    if (!max) { throw InvalidNodeError{{IndexedTracePoint{"range", 1}}}; }

    return std::make_unique<RandomMutationParameters>(
        SHO::RealFunctionDomain{*min, *max}, problem_parameters.get_domain());
}

}

namespace permutation_vector {

std::unique_ptr<MutationParameters>
load_swap_mutation_parameters(const ProblemParameters& /*problem_parameters*/,
                              const toml::table& /*mutation*/)
{
    return std::make_unique<SwapMutationParameters>();
}

std::unique_ptr<MutationParameters>
load_random_mutation_parameters(const ProblemParameters& /*problem_parameters*/,
                                const toml::table& /*mutation*/)
{
    return std::make_unique<RandomMutationParameters>();
}

}

}
