#include "SHO/Solution.hpp"

#include "SHO/Assert.hpp"
#include "SHO/Random.hpp"

#include <climits>
#include <cstring>
#include <algorithm>
#include <array>

namespace SHO {

namespace FloatingPoint {

double ArithmeticCrossover::mate(const double& first_parent, const double& second_parent)
{
    return (first_parent + second_parent) / 2.0;
}

RandomMutation::RandomMutation(double lower_bound, double upper_bound) : dist(lower_bound, upper_bound)
{
    SHO_ASSERT_FMT(lower_bound < upper_bound,
                   "Lower bound ({}) must be lower than the upper bound ({}).",
                   lower_bound,
                   upper_bound);
}

void RandomMutation::mutate(double& solution)
{
    solution = dist(Random::get_generator());
}

GaussianMutation::GaussianMutation(double standard_deviation) noexcept : standard_deviation(standard_deviation)
{
    SHO_ASSERT_FMT(
        standard_deviation > 0.0, "Standard deviation must be greater than zero ({} is provided).", standard_deviation);
}

void GaussianMutation::mutate(double& solution)
{
    std::normal_distribution<double> dist(solution, standard_deviation);
    solution = dist(Random::get_generator());
}

PositiveAdvancement::PositiveAdvancement(double delta, RealFunctionDomain domain) noexcept
        : delta(delta), domain(domain)
{
    SHO_ASSERT_FMT(delta > 0.0, "Delta must be greater than zero ({} is provided).", delta);
}

bool PositiveAdvancement::operator()(Solution& solution)
{
    SHO_ASSERT_FMT(domain.contains(solution), "Domain {} does not contain solution {}.", domain, solution);
    if (solution + delta >= domain.max) { return false; }
    solution += delta;
    return true;
}

std::vector<double> SimpleNeighborhoodProvider::get_neighborhood(const double& solution)
{
    return {solution / 2, solution / 4, solution / 8, solution / 16};
}

}

namespace BinaryVector {

Solution UniformCrossover::mate(const Solution& first_parent, const Solution& second_parent)
{
    SHO_ASSERT_FMT(first_parent.size() == second_parent.size(),
                   "Parents not the same size (first parent size: {}, "
                   "second_parent_size: {})",
                   first_parent.size(),
                   second_parent.size());

    Solution child(first_parent);
    std::bernoulli_distribution coin_flip;
    for (std::size_t i = 0; i < child.size(); i++) {
        if (coin_flip(Random::get_generator())) { child[i] = second_parent[i]; }
    }
    return child;
}

Solution SinglePointCrossover::mate(const Solution& first_parent, const Solution& second_parent)
{
    SHO_ASSERT_FMT(first_parent.size() == second_parent.size(),
                   "Parents not the same size (first parent size: {}, "
                   "second_parent_size: {})",
                   first_parent.size(),
                   second_parent.size());

    Solution child(first_parent);
    std::uniform_int_distribution<std::size_t> dist(0, child.size());
    for (auto i = dist(Random::get_generator()); i < child.size(); i++) { child[i] = second_parent[i]; }
    return child;
}

void RandomMutation::mutate(Solution& solution)
{
    std::bernoulli_distribution coin_flip;
    for (auto bit : solution) { bit = coin_flip(Random::get_generator()); }
}

void RandomFlipMutation::mutate(Solution& solution)
{
    std::uniform_int_distribution<std::size_t> dist(0, solution.size());
    solution[dist(Random::get_generator())].flip();
}

}

namespace PermutationVector {

namespace {

std::size_t find_element_index(const Solution& solution, Element element) noexcept
{
    const auto it = std::find(solution.begin(), solution.end(), element);
    SHO_ASSERT_FMT(it != solution.end(), "Solution does not contain element '{}'.", element);
    return std::distance(solution.begin(), it);
}

}

Solution CycleCrossover::mate(const Solution& first_parent, const Solution& second_parent)
{
    SHO_ASSERT_FMT(first_parent.size() == second_parent.size(),
                   "Parents not the same size (first parent size: {}, "
                   "second_parent_size: {})",
                   first_parent.size(),
                   second_parent.size());
    SHO_ASSERT(std::is_permutation(first_parent.begin(), first_parent.end(), second_parent.begin()),
               "Parents are not a permutation of one another.");

    const auto& size = first_parent.size();
    child_data.clear();
    child_data.resize(size, std::nullopt);
    const auto* source = &first_parent;
    const auto* other  = &second_parent;
    for (std::size_t i = 0; i < size;) {
        auto el       = (*source)[i];
        child_data[i] = el;

        std::size_t j;
        do {
            j             = find_element_index(*other, el);
            el            = (*source)[j];
            child_data[j] = el;
        } while (j != i);
        while (i < size && child_data[i]) { i++; }
        std::swap(source, other);
    }

    Solution solution;
    solution.reserve(size);
    std::transform(child_data.begin(),
                   child_data.end(),
                   std::back_inserter(solution),
                   [](const std::optional<Element>& element) noexcept {
                       SHO_ASSERT(element, "Not all child data has been set.");
                       return *element;
                   });
    return solution;
}

Solution TwoPointCrossover::mate(const Solution& first_parent, const Solution& second_parent)
{
    SHO_ASSERT_FMT(first_parent.size() == second_parent.size(),
                   "Parents not the same size (first parent size: {}, "
                   "second_parent_size: {})",
                   first_parent.size(),
                   second_parent.size());
    SHO_ASSERT(std::is_permutation(first_parent.begin(), first_parent.end(), second_parent.begin()),
               "Parents are not a permutation of one another.");

    const auto& size = first_parent.size();
    return mate(first_parent, second_parent, get_crossover_points(size));
}

auto TwoPointCrossover::get_crossover_points(std::size_t size) noexcept -> CrossoverPoints
{
    std::uniform_int_distribution<std::size_t> min_dist(0, size - 1);
    std::uniform_int_distribution<std::size_t> max_dist(0, size);
    auto min_cp = min_dist(Random::get_generator());
    auto max_cp = max_dist(Random::get_generator());
    if (min_cp > max_cp) { std::swap(min_cp, max_cp); }
    return {min_cp, max_cp};
}

Solution PartiallyMappedCrossover::mate(const Solution& first_parent,
                                        const Solution& second_parent,
                                        CrossoverPoints crossover_points)
{
    const auto [min_cp, max_cp] = crossover_points;
    const auto& size            = first_parent.size();

    SHO_ASSERT_FMT(min_cp <= max_cp,
                   "Min crossover point ({}) must be smaller or equal to max "
                   "crossover point ({}).",
                   min_cp,
                   max_cp);

    SHO_ASSERT_FMT(min_cp < size, "Min crossover point ({}) must be smaller than size ({}).", min_cp, size);
    SHO_ASSERT_FMT(max_cp <= size, "Max crossover point ({}) must be smaller or equal to size ({}).", max_cp, size);

    if (min_cp == max_cp) { return second_parent; }

    Solution child(size);
    const auto cmin = child.begin() + min_cp;
    const auto cmax = child.begin() + max_cp;
    std::copy(first_parent.begin() + min_cp, first_parent.begin() + max_cp, cmin);

    // Create a copy of second parent data and avoid allocation since vector can
    // reuse memory on copy assignment.
    second_parent_buffer = second_parent;
    const auto spmin     = second_parent_buffer.begin() + min_cp;
    const auto spmax     = second_parent_buffer.begin() + max_cp;

    auto it = spmin;
    for (auto cur = second_parent_buffer.begin(); cur != second_parent_buffer.end(); ++cur) {
        if (cur == spmin) {
            cur = spmax - 1; // Skip the first parent section.
            continue;
        }
        if (std::find(cmin, cmax, *cur) != cmax) {
            // Find first element between it and spmax that is not contained in
            // the first parent section.
            it = std::find_if(it, spmax, [&](const Element& el) noexcept { return std::find(cmin, cmax, el) == cmax; });
            SHO_ASSERT_FMT(
                it != second_parent_buffer.begin() + max_cp, "Element {} not found in the first parent segment.", *it);
            std::swap(*cur, *it);
            it++;
        }
    }

    std::copy(second_parent_buffer.begin(), second_parent_buffer.begin() + min_cp, child.begin());
    std::copy(second_parent_buffer.begin() + max_cp, second_parent_buffer.end(), child.begin() + max_cp);
    return Solution(child);
}

Solution
OrderCrossover::mate(const Solution& first_parent, const Solution& second_parent, CrossoverPoints crossover_points)
{
    const auto [min_cp, max_cp] = crossover_points;
    const auto& size            = first_parent.size();

    SHO_ASSERT_FMT(min_cp <= max_cp,
                   "Min crossover point ({}) must be smaller or equal to max "
                   "crossover point ({}).",
                   min_cp,
                   max_cp);

    SHO_ASSERT_FMT(min_cp < size, "Min crossover point ({}) must be smaller than size ({}).", min_cp, size);
    SHO_ASSERT_FMT(max_cp <= size, "Max crossover point ({}) must be smaller or equal to size ({}).", max_cp, size);

    if (min_cp == max_cp) { return second_parent; }

    Solution child(size);
    const auto cmin = child.begin() + min_cp;
    const auto cmax = child.begin() + max_cp;
    std::copy(first_parent.begin() + min_cp, first_parent.begin() + max_cp, cmin);

    auto child_it = cmin == child.begin() ? cmax : child.begin();
    for (std::size_t i = 0; i < size; i++) {
        const auto& el = second_parent[(max_cp + i) % size];
        if (std::find(cmin, cmax, el) == cmax) {
            *child_it = el;
            if (++child_it == cmin) { child_it = cmax; }
        }
    }

    return child;
}

void SwapMutation::mutate(Solution& solution)
{
    std::uniform_int_distribution<std::size_t> dist(0, solution.size() - 1);
    const auto i = dist(Random::get_generator());
    const auto j = dist(Random::get_generator());
    std::swap(solution[i], solution[j]);
}

void RandomMutation::mutate(Solution& solution)
{
    std::shuffle(solution.begin(), solution.end(), Random::get_generator());
}

std::vector<Solution> SwapNeighborhoodProvider::get_neighborhood(const Solution& solution)
{
    std::vector<Solution> neighborhood;
    neighborhood.reserve(solution.size() - 1);
    std::uniform_int_distribution<std::size_t> dist(0, solution.size() - 1);
    const auto chosen = dist(Random::get_generator());
    for (std::size_t i = 0; i < solution.size(); i++) {
        if (i != chosen) {
            auto solution_copy = solution;
            std::swap(solution_copy[i], solution_copy[chosen]);
            neighborhood.push_back(std::move(solution_copy));
        }
    }
    return neighborhood;
}

}

}

std::ostream& operator<<(std::ostream& os, const std::vector<bool>& s)
{
    for (const auto& bit : s) { os << bit; }
    return os;
}

std::ostream& operator<<(std::ostream& os, const std::vector<unsigned>& s)
{
    if (s.empty()) { return os << "{}"; }
    os << '{';
    for (std::size_t i = 0; i < s.size() - 1; i++) { os << s[i] << ", "; }
    return os << s.back() << '}';
}
