#ifndef SHO_TUNE_TERMINATIONCRITERIONPARAMETERS_HPP
#define SHO_TUNE_TERMINATIONCRITERIONPARAMETERS_HPP

#include <SHO/TerminationCriterion.hpp>

#include <memory>

namespace tune {

class TerminationCriterionParameters {
  public:
    virtual ~TerminationCriterionParameters() = default;

    virtual std::unique_ptr<SHO::TerminationCriterion> load() const = 0;
};

class MaxRunningTimeParameters : public TerminationCriterionParameters {
  private:
    std::chrono::duration<double> duration;

  public:
    explicit MaxRunningTimeParameters(
        std::chrono::duration<double> duration) noexcept;

    std::unique_ptr<SHO::TerminationCriterion> load() const final;

    std::chrono::duration<double> get_duration() const noexcept;
};

class MaxEvaluationsParameters : public TerminationCriterionParameters {
  private:
    unsigned long long max_evaluations;

  public:
    explicit MaxEvaluationsParameters(
        unsigned long long max_evaluations) noexcept;

    std::unique_ptr<SHO::TerminationCriterion> load() const final;

    unsigned long long get_max_evaluations() const noexcept;
};

class MaxIterationsParameters : public TerminationCriterionParameters {
  private:
    unsigned long long max_iterations;

  public:
    explicit MaxIterationsParameters(
        unsigned long long max_iterations) noexcept;

    std::unique_ptr<SHO::TerminationCriterion> load() const final;

    unsigned long long get_max_iterations() const noexcept;
};

class MaxIterationsWithoutImprovementParameters
        : public TerminationCriterionParameters {
  private:
    unsigned long long max_iterations_without_improvement;

  public:
    explicit MaxIterationsWithoutImprovementParameters(
        unsigned long long max_iterations_without_improvement) noexcept;

    std::unique_ptr<SHO::TerminationCriterion> load() const final;

    unsigned long long get_max_iterations_without_improvement() const noexcept;
};

}

#endif
