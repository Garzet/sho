#ifndef SHO_TUNE_INSPECTOR_HPP
#define SHO_TUNE_INSPECTOR_HPP

#include <SHO/State.hpp>
#include <SHO/SO/SSGA.hpp>

#include <iomanip>
#include <cassert>
#include <sstream>
#include <optional>

namespace tune {

template<typename Solution>
class SSGAInspector {
  public:
    using Population = SHO::SO::Population<Solution>;
    using State      = SHO::State;
    using Result     = typename SHO::SO::SSGA<Solution>::Result;

  public:
    virtual ~SSGAInspector() = default;

    virtual void inspect_iteration(const Population& population,
                                   const State& state) = 0;
};

template<typename Solution>
class NullSSGAInspector final : public SSGAInspector<Solution> {
  public:
    using Population = SHO::SO::Population<Solution>;
    using State      = SHO::State;
    using Result     = typename SHO::SO::SSGA<Solution>::Result;

  public:
    void inspect_iteration(const Population& population,
                           const State& state) final;
};

template<typename Solution>
class LogSSGAInspector : public SSGAInspector<Solution> {
  public:
    using Population = SHO::SO::Population<Solution>;
    using State      = SHO::State;
    using Result     = typename SHO::SO::SSGA<Solution>::Result;

  private:
    std::ostream& log_stream;
    std::chrono::duration<double> log_frequency;
    std::chrono::duration<double> previous_log_timepoint;

  public:
    LogSSGAInspector(std::ostream& log_stream,
                     std::chrono::duration<double> log_frequency);

    void inspect_iteration(const Population& population,
                           const State& state) override;
};

template<typename Solution>
void NullSSGAInspector<Solution>::inspect_iteration(
    const Population& /*population*/, const State& /*state*/)
{}

template<typename Solution>
void SSGAInspector<Solution>::inspect_iteration(
    const Population& /*population*/, const State& /*state*/)
{}

template<typename Solution>
LogSSGAInspector<Solution>::LogSSGAInspector(
    std::ostream& log_stream, std::chrono::duration<double> log_frequency)
        : log_stream{log_stream},
          log_frequency{log_frequency},
          previous_log_timepoint{0.0}
{}

template<typename Solution>
void LogSSGAInspector<Solution>::inspect_iteration(const Population& population,
                                                   const State& state)
{
    if (state.running_time - previous_log_timepoint > log_frequency ||
        state.iteration == 0) {
        log_stream << "Iteration: " << std::setw(8) << state.iteration
                   << ",    "
                   << "Best fitness: " << population.front().get_fitness()
                   << '\n';
        previous_log_timepoint = state.running_time;
    }
}

}

#endif
