#ifndef SHO_ASSERT_HPP
#define SHO_ASSERT_HPP

#include <fmt/core.h>
#include <fmt/color.h>
#include <fmt/ostream.h>
#include <cstdlib>

#ifndef NDEBUG
    #define SHO_ASSERT(expression, message)                   \
        do {                                                  \
            if (!(expression)) {                              \
                ::fmt::print(stderr,                          \
                             fg(fmt::color::red),             \
                             "[ASSERTION FAILED] ({}:{}) {}", \
                             __FILE__,                        \
                             __LINE__,                        \
                             message);                        \
                ::fmt::print(stderr, "\n");                   \
                ::std::abort();                               \
            }                                                 \
        } while (false)

    #define SHO_ASSERT_FMT(expression, format_string, ...)               \
        do {                                                             \
            if (!(expression)) {                                         \
                ::fmt::print(stderr,                                     \
                             fg(fmt::color::red),                        \
                             "[ASSERTION FAILED] ({}:{}) {}",            \
                             __FILE__,                                   \
                             __LINE__,                                   \
                             ::fmt::format(format_string, __VA_ARGS__)); \
                ::fmt::print(stderr, "\n");                              \
                ::std::abort();                                          \
            }                                                            \
        } while (false)

    #define SHO_ASSERT_UNREACHABLE(message)                            \
        do {                                                           \
            ::fmt::print(stderr,                                       \
                         fg(fmt::color::red),                          \
                         "[UNREACHABLE STATEMENT REACHED] ({}:{}) {}", \
                         __FILE__,                                     \
                         __LINE__,                                     \
                         message);                                     \
            ::fmt::print(stderr, "\n");                                \
            ::std::abort();                                            \
        } while (false)

    #define SHO_ASSERT_UNREACHABLE_FMT(format_string, ...)             \
        do {                                                           \
            ::fmt::print(stderr,                                       \
                         fg(fmt::color::red),                          \
                         "[UNREACHABLE STATEMENT REACHED] ({}:{}) {}", \
                         __FILE__,                                     \
                         __LINE__,                                     \
                         ::fmt::format(format_string, __VA_ARGS__));   \
            ::fmt::print(stderr, "\n");                                \
            ::std::abort();                                            \
        } while (false)

#else
    #define SHO_ASSERT(expression, message)
    #define SHO_ASSERT_FMT(expression, format_string, ...)
    #define SHO_ASSERT_UNREACHABLE(message)          ::std::abort()
    #define SHO_ASSERT_UNREACHABLE_FMT(message, ...) ::std::abort()
#endif

#endif
