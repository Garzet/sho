#ifndef SHO_SO_SSGA_HPP
#define SHO_SO_SSGA_HPP

#include "SHO/Random.hpp"
#include "SHO/Assert.hpp"
#include "SHO/State.hpp"
#include "SHO/Mutation.hpp"
#include "SHO/Crossover.hpp"
#include "SHO/TerminationCriterion.hpp"

#include "SHO/SO/Evaluator.hpp"
#include "SHO/SO/Individual.hpp"
#include "SHO/SO/Population.hpp"

#include <functional>

namespace SHO::SO {

template<typename Solution>
class SSGA {
  public:
    using State      = SHO::State;
    using Crossover  = SHO::Crossover<Solution>;
    using Mutation   = SHO::Mutation<Solution>;
    using Evaluator  = SHO::SO::Evaluator<Solution>;
    using Individual = SHO::SO::Individual<Solution>;
    using Population = SHO::SO::Population<Solution>;

  public:
    class Selection {
      public:
        using PopulationConstIterator = typename Population::const_iterator;

        struct Result {
            const Solution& first_parent;
            const Solution& second_parent;
            PopulationConstIterator individual_to_eliminate;
        };

      public:
        virtual ~Selection() = default;

        virtual Result select(const Population& population) = 0;
    };

    struct Result {
        Population final_population;
        const TerminationCriterion& met_criterion;
        State state;
    };

    using InspectionFunction =
        std::function<void(const Population& population, const State& state)>;

    class TournamentSelection : public Selection {
      public:
        using typename Selection::Result;

      private:
        std::size_t population_size;
        std::size_t tournament_size;
        std::vector<std::size_t> index_vector;
        std::vector<std::size_t> selected_indices;

      public:
        TournamentSelection(std::size_t population_size,
                            std::size_t tournament_size) noexcept;

        Result select(const Population& population) override;
    };

    class RouletteWheel : public Selection {
      public:
        using typename Selection::Result;
        using typename Selection::PopulationConstIterator;

      private:
        std::size_t population_size;
        std::vector<double> weights;

      public:
        RouletteWheel(std::size_t population_size) noexcept;

        Result select(const Population& population) override;
    };

  private:
    std::unique_ptr<Evaluator> evaluator;
    std::vector<std::unique_ptr<TerminationCriterion>> termination_criteria;
    Random::ProbabilityVector<std::unique_ptr<Selection>> selections;
    Random::ProbabilityVector<std::unique_ptr<Crossover>> crossovers;
    Random::ProbabilityVector<std::unique_ptr<Mutation>> mutations;
    double mutation_probability;

  public:
    SSGA(
        std::unique_ptr<Evaluator> evaluator,
        std::vector<std::unique_ptr<TerminationCriterion>> termination_criteria,
        Random::ProbabilityVector<std::unique_ptr<Selection>> selections,
        Random::ProbabilityVector<std::unique_ptr<Crossover>> crossovers,
        Random::ProbabilityVector<std::unique_ptr<Mutation>> mutations,
        double mutation_probability)
    noexcept;

    Result run(
        std::vector<Solution> initial_solutions,
        InspectionFunction inspection_function =
            [](const Population& /*population*/,
               const State& /*state*/) noexcept {});

  private:
    const TerminationCriterion*
    check_termination_critera(const State& state) const;

    static void insert_into_population(Population& population,
                                       Individual individual);

    static void update_algorithm_state(
        double& best_fitness,
        State& state,
        const std::chrono::high_resolution_clock::time_point& start_timepoint,
        const Population& population) noexcept;
};

template<typename Solution>
SSGA<Solution>::SSGA(
    std::unique_ptr<Evaluator> evaluator,
    std::vector<std::unique_ptr<TerminationCriterion>> termination_criteria,
    Random::ProbabilityVector<std::unique_ptr<Selection>> selections,
    Random::ProbabilityVector<std::unique_ptr<Crossover>> crossovers,
    Random::ProbabilityVector<std::unique_ptr<Mutation>> mutations,
    double mutation_probability) noexcept
        : evaluator(std::move(evaluator)),
          termination_criteria(std::move(termination_criteria)),
          selections(std::move(selections)),
          crossovers(std::move(crossovers)),
          mutations(std::move(mutations)),
          mutation_probability(mutation_probability)
{
    SHO_ASSERT(this->evaluator, "Evaluator may not be nullptr.");
    SHO_ASSERT_FMT(mutation_probability >= 0.0 && mutation_probability <= 1.0,
                   "Invalid mutation probability ({}).",
                   mutation_probability);
}

template<typename Solution>
auto SSGA<Solution>::run(std::vector<Solution> initial_solutions,
                         InspectionFunction inspection_function) -> Result
{
    SHO_ASSERT(!initial_solutions.empty(),
               "At least one initial solution must be provided.");

    Population population(std::move(initial_solutions));

    // Evaluate and sort initial population so that the best solutions are at
    // the front of the population.
    for (auto& individual : population) {
        individual.set_fitness(evaluator->evaluate(individual.solution));
    }
    population.sort();
    double best_fitness = population.front().get_fitness();

    State state{0,
                0,
                population.size(), // Entire initial population is evaluated.
                std::chrono::seconds(0)};

    inspection_function(population, state);

    const auto start_timepoint = std::chrono::high_resolution_clock::now();
    const TerminationCriterion* met_criterion = nullptr;
    while (!(met_criterion = check_termination_critera(state))) {
        Selection& selection = *selections.get_random_element();
        auto selected        = selection.select(population);

        Crossover& crossover = *crossovers.get_random_element();
        auto child =
            crossover.mate(selected.first_parent, selected.second_parent);

        std::bernoulli_distribution mut_dist(mutation_probability);
        if (mut_dist(Random::get_generator())) {
            Mutation& mutation = *mutations.get_random_element();
            mutation.mutate(child);
        }

        Individual new_individual(std::move(child));
        new_individual.set_fitness(
            evaluator->evaluate(new_individual.solution));

        // Remove designated individual from the population and insert new
        // individual into population so that the population remains sorted.
        population.erase(selected.individual_to_eliminate);
        insert_into_population(population, std::move(new_individual));

        update_algorithm_state(
            best_fitness, state, start_timepoint, population);

        inspection_function(population, state);
    }

    return {std::move(population), *met_criterion, std::move(state)};
}

template<typename Solution>
const TerminationCriterion*
SSGA<Solution>::check_termination_critera(const State& state) const
{
    for (auto& termination_criterion : termination_criteria) {
        if (termination_criterion->is_met(state)) {
            return termination_criterion.get();
        }
    }
    return nullptr;
}

template<typename Solution>
void SSGA<Solution>::insert_into_population(Population& population,
                                            Individual individual)
{
    auto it =
        std::find_if(population.begin(),
                     population.end(),
                     [&individual](const Individual& i) {
                         return i.get_fitness() < individual.get_fitness();
                     });
    population.insert(it, std::move(individual));
}

template<typename Solution>
void SSGA<Solution>::update_algorithm_state(
    double& best_fitness,
    State& state,
    const std::chrono::high_resolution_clock::time_point& start_timepoint,
    const Population& population) noexcept
{
    state.iteration++;
    if (best_fitness >= population.front().get_fitness()) {
        state.n_iterations_without_improvement++;
    } else {
        state.n_iterations_without_improvement = 0;
        best_fitness = population.front().get_fitness();
    }
    state.n_evaluations++; // Single evaluation per iteration.
    state.running_time =
        std::chrono::high_resolution_clock::now() - start_timepoint;
}

template<typename Solution>
SSGA<Solution>::TournamentSelection::TournamentSelection(
    std::size_t population_size, std::size_t tournament_size) noexcept
        : population_size(population_size), tournament_size(tournament_size)
{
    [[maybe_unused]] constexpr static std::size_t min_tournament_size = 3;
    SHO_ASSERT_FMT(tournament_size >= min_tournament_size,
                   "Tournament size must be at least {} (provided: {}).",
                   min_tournament_size,
                   tournament_size);
    SHO_ASSERT_FMT(tournament_size <= population_size,
                   "Tournament size ({}) must be lower or equal to "
                   "population size ({}).",
                   tournament_size,
                   population_size);

    // Generate index vector.
    index_vector.reserve(population_size);
    for (std::size_t i = 0; i < population_size; i++) {
        index_vector.push_back(i);
    }

    selected_indices.reserve(tournament_size);
}

template<typename Solution>
auto SSGA<Solution>::TournamentSelection::select(const Population& population)
    -> Result
{
    SHO_ASSERT_FMT(population.size() == population_size,
                   "Tournament selection initialized with population size "
                   "equal to {}, but population of size {} was provided.",
                   population_size,
                   population.size());

    std::sample(index_vector.begin(),
                index_vector.end(),
                std::back_inserter(selected_indices),
                tournament_size,
                Random::get_generator());

    const auto comparator = [&population](std::size_t lhs,
                                          std::size_t rhs) noexcept {
        return population[lhs].get_fitness() < population[rhs].get_fitness();
    };

    // Find the best individual in the tournament.
    auto it = std::max_element(
        selected_indices.begin(), selected_indices.end(), comparator);
    const std::size_t best_index = *it;
    selected_indices.erase(it);

    // Find the second best individual in the tournament.
    it = std::max_element(
        selected_indices.begin(), selected_indices.end(), comparator);
    const std::size_t second_best_index = *it;

    // Find the worst individual in the tournament.
    it = std::min_element(
        selected_indices.begin(), selected_indices.end(), comparator);
    const std::size_t worst_index = *it;

    selected_indices.clear(); // Clear selected indices for next iteration.

    return {population[best_index].solution,
            population[second_best_index].solution,
            population.begin() + worst_index};
}

template<typename Solution>
SSGA<Solution>::RouletteWheel::RouletteWheel(
    std::size_t population_size) noexcept
        : population_size(population_size)
{
    SHO_ASSERT(population_size > 0, "Population size may not be zero.");
    weights.reserve(population_size);
}

template<typename Solution>
auto SSGA<Solution>::RouletteWheel::select(const Population& population)
    -> Result
{
    SHO_ASSERT_FMT(population_size == population.size(),
                   "Roulette wheel selection expects population of size {}, "
                   "but population of size {} was provided.",
                   population_size,
                   population.size());

    weights.clear();

    bool negative_fitness = true; // Assume minimization.
    for (const auto& individual : population) {
        double fitness = individual.get_fitness();
        if (fitness < 0.0) {
            negative_fitness = true;
            break;
        }
        if (fitness > 0.0) {
            negative_fitness = false;
            break;
        }
    }

    for (const auto& individual : population) {
        double fitness = individual.get_fitness();
        if ((negative_fitness && fitness > 0.0) ||
            (!negative_fitness && fitness < 0.0)) {
            throw std::runtime_error(
                fmt::format("Mixed fitness population encountered (expected {} "
                            "fitness values, but value {} was encountered).",
                            negative_fitness ? "negative" : "positive",
                            fitness));
        }
        weights.push_back(negative_fitness ? 1.0 / -fitness : fitness);
    }

    std::discrete_distribution<std::size_t> parent_dist(weights.begin(),
                                                        weights.end());

    const Solution& first_parent =
        population[parent_dist(Random::get_generator())].solution;
    const Solution& second_parent =
        population[parent_dist(Random::get_generator())].solution;

    // Inverse weights to select individual to eliminate.
    for (auto& weight : weights) { weight = 1.0 / weight; }
    std::discrete_distribution<std::size_t> elim_dist(weights.begin(),
                                                      weights.end());
    PopulationConstIterator individual_to_eliminate =
        population.begin() + elim_dist(Random::get_generator());

    return {first_parent, second_parent, individual_to_eliminate};
}

}

#endif
