#include "tune/AlgorithmParametersLoaderTOML.hpp"

#include "tune/Error.hpp"

namespace tune {

GeneticAlgorithmParameters
load_genetic_algorithm_parameters(const toml::table& algorithm)
{
    const auto mutation_probability =
        algorithm["mutation_probability"].value<double>();
    if (!mutation_probability) {
        throw InvalidNodeError{{BasicTracePoint{"mutation_probability"}}};
    }

    const auto population_size = algorithm["population_size"].value<unsigned>();
    if (!population_size) {
        throw InvalidNodeError{{BasicTracePoint{"population_size"}}};
    }

    return GeneticAlgorithmParameters(*mutation_probability, *population_size);
}

}
