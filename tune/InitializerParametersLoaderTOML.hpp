#ifndef SHO_TUNE_INITIALIZERPARAMETERSLOADERTOML_HPP
#define SHO_TUNE_INITIALIZERPARAMETERSLOADERTOML_HPP

#include "tune/InitializerParameters.hpp"
#include "tune/ProblemParameters.hpp"

#include <toml++/toml.h>

namespace tune {

namespace floating_point {

using PopulationInitializerParameters =
    PopulationInitializerParameters<Solution>;

std::unique_ptr<PopulationInitializerParameters>
load_random_population_initializer_parameters(
    unsigned population_size,
    const ProblemParameters& problem_parameters,
    const toml::table& initializer);

std::unique_ptr<PopulationInitializerParameters>
load_fixed_population_initializer_parameters(
    unsigned population_size,
    const ProblemParameters& problem_parameters,
    const toml::table& initializer);

}

namespace permutation_vector {

using PopulationInitializerParameters =
    PopulationInitializerParameters<Solution>;

std::unique_ptr<PopulationInitializerParameters>
load_random_population_initializer_parameters(
    unsigned population_size,
    const ProblemParameters& problem_parameters,
    const toml::table& initializer);

}

}

#endif
