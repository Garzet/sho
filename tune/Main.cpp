#include "tune/ExperimentParametersLoaderTOML.hpp"
#include "tune/GridSearchPreprocessorTOML.hpp"
#include "tune/InspectorParametersLoaderTOML.hpp"
#include "tune/Parallelization.hpp"

#include <future>
#include <iostream>
#include <cstdlib>

namespace tune {

namespace floating_point {

SSGAExperimentParametersLoaderTOML<ProblemParameters>
create_default_ssga_experiment_parameters_loader();

}

namespace permutation_vector {

SSGAExperimentParametersLoaderTOML<ProblemParameters>
create_default_ssga_experiment_parameters_loader();

}

}

int main()
{
    using namespace tune;

    const auto ssga_experiment_parameters_loader =
        permutation_vector::create_default_ssga_experiment_parameters_loader();

    using SSGAExperimentParameters =
        tune::SSGAExperimentParameters<permutation_vector::Solution>;

    std::vector<SSGAExperimentParameters> ssga_experiments_parameters;
    const auto table{toml::parse_file("../config.toml")};
    const auto& experiments_array{*table["experiments"].as_array()};

    std::vector<toml::table> preprocessed_experiments;
    try {
        preprocessed_experiments =
            GridSearchPreprocessorTOML::preprocess(experiments_array);
        ssga_experiments_parameters.reserve(preprocessed_experiments.size());
        for (const auto& experiment : preprocessed_experiments) {
            ssga_experiments_parameters.push_back(
                ssga_experiment_parameters_loader.load(experiment));
        }
    } catch (const TsplibUnsupportedEntryError& exc) {
        std::cout << "TSPLIB file unsupported entry:\n\t" << exc.get_key()
                  << ": " << exc.get_entry() << "\nSupported entries: ";
        for (const auto& supported_entry : exc.get_supported_entries()) {
            std::cout << supported_entry << ' ';
        }
        std::cout << '\n';
        return EXIT_FAILURE;
    } catch (const TraceError& trace_error) {
        std::cout << "Exception: " << trace_error.what() << '\n';
        const auto& trace{trace_error.get_trace()};
        if (!trace.empty()) {
            std::cout << "Trace:\n";
            for (std::size_t i{trace.size() - 1}; i > 0; --i) {
                std::visit(
                    [](const auto& t) { std::cout << '\t' << t << '\n'; },
                    trace[i]);
            }
            std::visit([](const auto& t) { std::cout << '\t' << t; },
                       trace.front());
        }
        std::cout << '\n';
        return EXIT_FAILURE;
    }

    static constexpr unsigned n_runs{1};
    static const unsigned n_threads{std::thread::hardware_concurrency()};
    tune::ThreadPool thread_pool{n_threads};
    auto ssga_results_futures{
        tune::parallelization::parallelize_ssga_experiments(
            ssga_experiments_parameters, n_runs, thread_pool)};

    std::vector<double> best_fitnesses;
    ssga_results_futures.reserve(ssga_results_futures.size() * n_runs);
    for (auto& ssga_result_future : ssga_results_futures) {
        auto ssga_result{ssga_result_future.get()};
        best_fitnesses.push_back(
            ssga_result.final_population.front().get_fitness());
    }

    std::vector<double> experiments_avg_best_fitness;
    experiments_avg_best_fitness.reserve(ssga_experiments_parameters.size());
    for (auto it{best_fitnesses.begin()}; it != best_fitnesses.end();
         std::advance(it, n_runs)) {
        double sum{std::accumulate(it, std::next(it, n_runs), 0.0)};
        experiments_avg_best_fitness.push_back(sum / n_runs);
    }

    const auto best_experiment_index{
        std::distance(experiments_avg_best_fitness.begin(),
                      std::max_element(experiments_avg_best_fitness.begin(),
                                       experiments_avg_best_fitness.end()))};

    const auto best_solution_index{std::distance(
        best_fitnesses.begin(),
        std::max_element(best_fitnesses.begin(), best_fitnesses.end()))};
    std::cout << "best found solution fitness:"
              << best_fitnesses[best_solution_index]
              << "\nbest found solution index: " << best_solution_index << '\n';
    std::cout << "experiments avgerage best fitness:\n";
    for (auto avg : experiments_avg_best_fitness) {
        std::cout << '\t' << avg << '\n';
    }
    std::cout << "best experiment (by best fitness average) index: "
              << best_experiment_index << '\n'
              << "--------- best experiment (by best fitness average) "
                 "parameters ---------\n"
              << preprocessed_experiments[best_experiment_index]
              << "\n-----------------------------------------------------------"
                 "-------------\n";
}

namespace tune {

namespace floating_point {

SSGAExperimentParametersLoaderTOML<ProblemParameters>
create_default_ssga_experiment_parameters_loader()
{
    SSGAExperimentParametersLoaderTOML<ProblemParameters> res;

    res.problem_parameters_loaders["Square"] = load_square_problem_parameters;

    res.selection_parameters_loaders["TournamentSelection"] =
        load_tournament_selection_parameters;
    res.selection_parameters_loaders["RouletteWheel"] =
        load_roulette_wheel_selection_parameters;

    res.crossover_parameters_loaders["ArithmeticCrossover"] =
        load_arithmetic_crossover_parameters;

    res.mutation_parameters_loaders["GaussianMutation"] =
        load_gaussian_mutation_parameters;
    res.mutation_parameters_loaders["RandomMutation"] =
        load_random_mutation_parameters;

    res.initializer_parameters_loaders["RandomInitializer"] =
        load_random_population_initializer_parameters;
    res.initializer_parameters_loaders["FixedInitializer"] =
        load_fixed_population_initializer_parameters;

    res.terminations_parameters_loaders["max_running_time"] =
        load_max_running_time;
    res.terminations_parameters_loaders["max_evaluations"] =
        load_max_evaluations;
    res.terminations_parameters_loaders["max_iterations"] = load_max_iterations;
    res.terminations_parameters_loaders["max_iterations_without_improvement"] =
        load_max_iterations_without_improvement;

    res.inspector_parameters_loaders["NullInspector"] =
        std::make_unique<NullSSGAInspectorParametersLoaderTOML<Solution>>();
    res.inspector_parameters_loaders["LogInspector"] =
        std::make_unique<LogSSGAInspectorParametersLoaderTOML<Solution>>();

    return res;
}

}

namespace permutation_vector {

SSGAExperimentParametersLoaderTOML<ProblemParameters>
create_default_ssga_experiment_parameters_loader()
{
    SSGAExperimentParametersLoaderTOML<ProblemParameters> res;

    res.problem_parameters_loaders["TravelingSalesman"] =
        load_traveling_salesman_problem_parameters;

    res.selection_parameters_loaders["TournamentSelection"] =
        load_tournament_selection_parameters;
    res.selection_parameters_loaders["RouletteWheel"] =
        load_roulette_wheel_selection_parameters;

    res.crossover_parameters_loaders["CycleCrossover"] =
        load_cycle_crossover_parameters;
    res.crossover_parameters_loaders["PartiallyMappedCrossover"] =
        load_partially_mapped_crossover_parameters;
    res.crossover_parameters_loaders["OrderCrossover"] =
        load_order_crossover_parameters;

    res.mutation_parameters_loaders["SwapMutation"] =
        load_swap_mutation_parameters;
    res.mutation_parameters_loaders["RandomMutation"] =
        load_random_mutation_parameters;

    res.initializer_parameters_loaders["RandomInitializer"] =
        load_random_population_initializer_parameters;

    res.terminations_parameters_loaders["max_running_time"] =
        load_max_running_time;
    res.terminations_parameters_loaders["max_evaluations"] =
        load_max_evaluations;
    res.terminations_parameters_loaders["max_iterations"] = load_max_iterations;
    res.terminations_parameters_loaders["max_iterations_without_improvement"] =
        load_max_iterations_without_improvement;

    res.inspector_parameters_loaders["NullInspector"] =
        std::make_unique<NullSSGAInspectorParametersLoaderTOML<Solution>>();
    res.inspector_parameters_loaders["LogInspector"] =
        std::make_unique<LogSSGAInspectorParametersLoaderTOML<Solution>>();

    return res;
}

}

}
