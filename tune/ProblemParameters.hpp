#ifndef SHO_TUNE_PROBLEMPARAMETERS_HPP
#define SHO_TUNE_PROBLEMPARAMETERS_HPP

#include "tune/Problem.hpp"

#include <memory>

namespace tune {

template<typename Solution>
class ProblemParameters {
  public:
    using Evaluator = SHO::SO::Evaluator<Solution>;

  public:
    virtual ~ProblemParameters() = default;

    virtual std::unique_ptr<Evaluator> load() const = 0;
};

namespace floating_point {

class ProblemParameters : public tune::ProblemParameters<Solution> {
  public:
    using Solution  = tune::floating_point::Solution;
    using Evaluator = tune::ProblemParameters<Solution>::Evaluator;

  private:
    SHO::RealFunctionDomain domain;

  public:
    explicit ProblemParameters(SHO::RealFunctionDomain domain);

    SHO::RealFunctionDomain get_domain() const noexcept;
};

class SquareProblemParameters final : public ProblemParameters {
  public:
    explicit SquareProblemParameters(SHO::RealFunctionDomain domain);

    std::unique_ptr<Evaluator> load() const final;
};

}

namespace permutation_vector {

class ProblemParameters : public tune::ProblemParameters<Solution> {
  public:
    using Solution  = tune::permutation_vector::Solution;
    using Evaluator = tune::ProblemParameters<Solution>::Evaluator;

  private:
    std::size_t solution_size;

  public:
    explicit ProblemParameters(std::size_t solution_size) noexcept;

    std::size_t get_solution_size() const noexcept;
};

class TravelingSalesmanProblemParameters final : public ProblemParameters {
  private:
    TravelingSalesmanProblem::DistanceMatrix distance_matrix;

  public:
    explicit TravelingSalesmanProblemParameters(
        TravelingSalesmanProblem::DistanceMatrix distance_matrix);

    std::unique_ptr<Evaluator> load() const final;
};

}

}

#endif
