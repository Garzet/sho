#ifndef SHO_TUNE_INSPECTORPARAMETERS_HPP
#define SHO_TUNE_INSPECTORPARAMETERS_HPP

#include "tune/Inspector.hpp"

#include <ostream>
#include <memory>
#include <optional>

namespace tune {

template<typename Solution>
class SSGAInspectorParameters {
  public:
    using SSGAInspector = tune::SSGAInspector<Solution>;

  public:
    virtual ~SSGAInspectorParameters() = default;

    virtual std::unique_ptr<SSGAInspector> load() const = 0;
};

template<typename Solution>
class NullSSGAInspectorParameters final
        : public SSGAInspectorParameters<Solution> {
  public:
    using SSGAInspector     = tune::SSGAInspector<Solution>;
    using NullSSGAInspector = tune::NullSSGAInspector<Solution>;

  public:
    std::unique_ptr<SSGAInspector> load() const final;
};

template<typename Solution>
class LogSSGAInspectorParameters : public SSGAInspectorParameters<Solution> {
  public:
    using SSGAInspector    = tune::SSGAInspector<Solution>;
    using LogSSGAInspector = tune::LogSSGAInspector<Solution>;

  private:
    std::ostream& os;
    std::chrono::duration<double> frequency;

  public:
    LogSSGAInspectorParameters(
        std::ostream& os, std::chrono::duration<double> frequency) noexcept;

    std::unique_ptr<SSGAInspector> load() const override;

    std::ostream& get_ostream() const noexcept;
    std::chrono::duration<double> get_frequency() const noexcept;
};

template<typename Solution>
auto NullSSGAInspectorParameters<Solution>::load() const
    -> std::unique_ptr<SSGAInspector>
{
    return std::make_unique<NullSSGAInspector>();
}

template<typename Solution>
LogSSGAInspectorParameters<Solution>::LogSSGAInspectorParameters(
    std::ostream& os, std::chrono::duration<double> frequency) noexcept
        : os{os}, frequency{frequency}
{}

template<typename Solution>
auto LogSSGAInspectorParameters<Solution>::load() const
    -> std::unique_ptr<SSGAInspector>
{
    return std::make_unique<LogSSGAInspector>(os, frequency);
}

template<typename Solution>
std::ostream& LogSSGAInspectorParameters<Solution>::get_ostream() const noexcept
{
    return os;
}

template<typename Solution>
std::chrono::duration<double>
LogSSGAInspectorParameters<Solution>::get_frequency() const noexcept
{
    return frequency;
}

}

#endif
