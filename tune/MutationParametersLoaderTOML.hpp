#ifndef SHO_TUNE_MUTATIONPARAMETERSLODAERTOML_HPP
#define SHO_TUNE_MUTATIONPARAMETERSLODAERTOML_HPP

#include "tune/MutationParameters.hpp"
#include "tune/ProblemParameters.hpp"

#include <toml++/toml.h>

namespace tune {

namespace floating_point {

std::unique_ptr<MutationParameters>
load_gaussian_mutation_parameters(const ProblemParameters& problem_parameters,
                                  const toml::table& mutation);

std::unique_ptr<MutationParameters>
load_random_mutation_parameters(const ProblemParameters& problem_parameters,
                                const toml::table& mutation);

}

namespace permutation_vector {

std::unique_ptr<MutationParameters>
load_swap_mutation_parameters(const ProblemParameters& problem_parameters,
                              const toml::table& mutation);

std::unique_ptr<MutationParameters>
load_random_mutation_parameters(const ProblemParameters& problem_parameters,
                                const toml::table& mutation);

}

}

#endif
