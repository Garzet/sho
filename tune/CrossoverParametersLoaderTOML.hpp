#ifndef SHO_TUNE_CROSSOVERPARAMETERSLOADERTOML_HPP
#define SHO_TUNE_CROSSOVERPARAMETERSLOADERTOML_HPP

#include "tune/ProblemParameters.hpp"
#include "tune/CrossoverParameters.hpp"

#include <toml++/toml.h>

namespace tune {

namespace floating_point {

using CrossoverParameters = tune::CrossoverParameters<Solution>;

std::unique_ptr<CrossoverParameters> load_arithmetic_crossover_parameters(
    const ProblemParameters& problem_parameters, const toml::table& crossover);

}

namespace permutation_vector {

using CrossoverParameters = tune::CrossoverParameters<Solution>;

std::unique_ptr<CrossoverParameters>
load_cycle_crossover_parameters(const ProblemParameters& problem_parameters,
                                const toml::table& crossover);

std::unique_ptr<CrossoverParameters> load_partially_mapped_crossover_parameters(
    const ProblemParameters& problem_parameters, const toml::table& crossover);

std::unique_ptr<CrossoverParameters>
load_order_crossover_parameters(const ProblemParameters& problem_parameters,
                                const toml::table& crossover);

}

}

#endif
