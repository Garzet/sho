#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_floating_point.hpp>

#include "SHO/MO/Population.hpp"

TEST_CASE("CrowdingSort")
{
    constexpr static std::size_t n_objectives = 2;
    using Population                          = SHO::MO::Population<int, n_objectives>;
    using FitnessDomain                       = Population::FitnessDomain;

    constexpr FitnessDomain domain = {SHO::RealFunctionDomain{-10, 0}, SHO::RealFunctionDomain{-100, 0}};

    Population population;
    population.reserve(3);

    population.push_back({1, {-1, -30}});
    population.push_back({2, {-2, -20}});
    population.push_back({3, {-4, -10}});

    auto fronts = population.nondominated_sort();
    REQUIRE(fronts.size() == 1);

    auto& front = fronts[0];
    REQUIRE(front.size() == 3);

    front.crowding_sort(domain);
    REQUIRE(std::isinf(front[0].get_crowding_distance()));
    REQUIRE(std::isinf(front[1].get_crowding_distance()));
    REQUIRE_THAT(front[2].get_crowding_distance(), Catch::Matchers::WithinRel(0.5, 0.001));
}
