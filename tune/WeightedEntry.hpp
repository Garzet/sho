#ifndef SHO_TUNE_WEIGHTEDENTRY_HPP
#define SHO_TUNE_WEIGHTEDENTRY_HPP

#include "tune/Error.hpp"

#include "SHO/Random.hpp"

#include <vector>
#include <sstream>

namespace tune {

template<typename T>
class WeightedEntry {
  private:
    T entry;
    double weight;

  public:
    WeightedEntry(T entry, double weight);

    const T& get_entry() const noexcept;
    double get_weight() const noexcept;
};

template<typename T>
WeightedEntry<T>::WeightedEntry(T entry, double weight)
        : entry{std::move(entry)}, weight{weight}
{
    if (weight <= 0.0) { throw InvalidWeightError{weight}; }
}

template<typename T>
const T& WeightedEntry<T>::get_entry() const noexcept
{
    return entry;
}

template<typename T>
double WeightedEntry<T>::get_weight() const noexcept
{
    return weight;
}

template<typename ParametersType>
class WeightedVector
        : public std::vector<WeightedEntry<std::unique_ptr<ParametersType>>> {
  private:
    using Result = decltype(std::declval<ParametersType>().load());

  public:
    SHO::Random::ProbabilityVector<Result> load_probability_vector() const;
};

template<typename T>
auto WeightedVector<T>::load_probability_vector() const
    -> SHO::Random::ProbabilityVector<Result>
{
    std::vector<Result> elements;
    elements.reserve(this->size());
    std::vector<double> weights;
    weights.reserve(this->size());

    for (const auto& i : *this) {
        elements.push_back(i.get_entry()->load());
        weights.push_back(i.get_weight());
    }

    return SHO::Random::ProbabilityVector<Result>(std::move(elements),
                                                  std::move(weights));
}

}

#endif
