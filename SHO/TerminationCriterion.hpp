#ifndef SHO_TERMINATIONCRITERION_HPP
#define SHO_TERMINATIONCRITERION_HPP

#include "SHO/State.hpp"

#include <string>
#include <vector>
#include <memory>
#include <functional>

namespace SHO {

class TerminationCriterion {
  public:
    virtual ~TerminationCriterion() noexcept = default;

    [[nodiscard]] virtual bool is_met(const State& state)                = 0;
    [[nodiscard]] virtual std::string format_termination_message() const = 0;

  protected:
    TerminationCriterion() noexcept                                             = default;
    TerminationCriterion(const TerminationCriterion& other) noexcept            = default;
    TerminationCriterion& operator=(const TerminationCriterion& other) noexcept = default;
    TerminationCriterion(TerminationCriterion&& other) noexcept                 = default;
    TerminationCriterion& operator=(TerminationCriterion&& other) noexcept      = default;
};

[[nodiscard]] const TerminationCriterion*
check_termination_critera(const std::vector<std::reference_wrapper<TerminationCriterion>>& termination_criteria,
                          const State& state);

class MaxIterations final : public TerminationCriterion {
  private:
    unsigned long long max_iterations;

  public:
    explicit MaxIterations(unsigned long long max_iterations) noexcept;

    [[nodiscard]] bool is_met(const State& state) final;
    [[nodiscard]] std::string format_termination_message() const final;

    [[nodiscard]] unsigned long long get_max_iterations() const noexcept;
};

class MaxIterationsWithoutImprovement final : public TerminationCriterion {
  private:
    unsigned long long max_iterations_without_improvement;

  public:
    explicit MaxIterationsWithoutImprovement(unsigned long long max_iterations_without_improvement) noexcept;

    [[nodiscard]] bool is_met(const State& state) final;
    [[nodiscard]] std::string format_termination_message() const final;

    [[nodiscard]] unsigned long long get_max_iterations_without_improvement() const noexcept;
};

class MaxEvaluations final : public TerminationCriterion {
  private:
    unsigned long long max_evaluations;

  public:
    explicit MaxEvaluations(unsigned long long max_evaluations) noexcept;

    [[nodiscard]] bool is_met(const State& state) override;
    [[nodiscard]] std::string format_termination_message() const final;

    [[nodiscard]] unsigned long long get_max_evaluations() const noexcept;
};

class MaxRunningTime final : public TerminationCriterion {
  private:
    std::chrono::duration<double> max_running_time;

  public:
    explicit MaxRunningTime(std::chrono::duration<double> max_running_time) noexcept;

    [[nodiscard]] bool is_met(const State& state) override;
    [[nodiscard]] std::string format_termination_message() const override;

    [[nodiscard]] std::chrono::duration<double> get_max_running_time() const noexcept;
};

}

#endif
