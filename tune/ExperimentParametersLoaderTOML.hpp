#ifndef SHO_TUNE_EXPERIMENTPARAMETERSLOADERTOML_HPP
#define SHO_TUNE_EXPERIMENTPARAMETERSLOADERTOML_HPP

#include "tune/ExperimentParameters.hpp"
#include "tune/AlgorithmParametersLoaderTOML.hpp"
#include "tune/InitializerParametersLoaderTOML.hpp"
#include "tune/InspectorParametersLoaderTOML.hpp"
#include "tune/MutationParametersLoaderTOML.hpp"
#include "tune/ProblemParametersLoaderTOML.hpp"
#include "tune/SelectionParametersLoaderTOML.hpp"
#include "tune/CrossoverParametersLoaderTOML.hpp"
#include "tune/TerminationCriterionParametersLoaderTOML.hpp"
#include "tune/Error.hpp"

#include <toml++/toml.h>

namespace tune {

class ExperimentParametersLoaderTOML {
  public:
    template<typename Loader>
    using LoaderMap = std::unordered_map<std::string, Loader>;

  public:
    LoaderMap<TerminationCriterionParametersLoaderTOML>
        terminations_parameters_loaders;

  protected:
    explicit ExperimentParametersLoaderTOML() = default;

    std::vector<std::unique_ptr<TerminationCriterionParameters>>
    load_termination_criteria_parameters(const toml::table& terminations) const;

  protected:
    static std::string_view load_problem_name(const toml::table& experiment);
    static std::string_view load_algorithm_name(const toml::table& experiment);

    static std::optional<unsigned> load_seed(const toml::table& algorithm);
};

template<typename ProblemParameters>
class SSGAExperimentParametersLoaderTOML
        : public ExperimentParametersLoaderTOML {
  public:
    using Solution                 = typename ProblemParameters::Solution;
    using SSGAExperimentParameters = tune::SSGAExperimentParameters<Solution>;

    using ProblemParametersLoaderTOML =
        std::unique_ptr<ProblemParameters> (*)(const toml::table& problem);

    using SelectionParameters = tune::SelectionParameters<Solution>;
    using SelectionParametersLoaderTOML =
        std::unique_ptr<SelectionParameters> (*)(
            const GeneticAlgorithmParameters& ga_parameters,
            const toml::table& selection);

    using CrossoverParameters = tune::CrossoverParameters<Solution>;
    using CrossoverParametersLoaderTOML =
        std::unique_ptr<CrossoverParameters> (*)(
            const ProblemParameters& problem_parameters,
            const toml::table& crossover);

    using MutationParameters = tune::MutationParameters<Solution>;
    using MutationParametersLoaderTOML =
        std::unique_ptr<MutationParameters> (*)(
            const ProblemParameters& problem_parameters,
            const toml::table& mutation);

    using PopulationInitializerParameters =
        tune::PopulationInitializerParameters<Solution>;
    using PopulationInitializerParametersLoaderTOML =
        std::unique_ptr<PopulationInitializerParameters> (*)(
            unsigned population_size,
            const ProblemParameters& problem_parameters,
            const toml::table& initializer);

    using SSGAInspectorParameters = tune::SSGAInspectorParameters<Solution>;
    using SSGAInspectorParametersLoaderTOML =
        tune::SSGAInspectorParametersLoaderTOML<Solution>;

  public:
    LoaderMap<ProblemParametersLoaderTOML> problem_parameters_loaders;

    LoaderMap<SelectionParametersLoaderTOML> selection_parameters_loaders;
    LoaderMap<CrossoverParametersLoaderTOML> crossover_parameters_loaders;
    LoaderMap<MutationParametersLoaderTOML> mutation_parameters_loaders;

    LoaderMap<PopulationInitializerParametersLoaderTOML>
        initializer_parameters_loaders;
    LoaderMap<std::unique_ptr<SSGAInspectorParametersLoaderTOML>>
        inspector_parameters_loaders;

  public:
    SSGAExperimentParameters load(const toml::table& experiment) const;

  private:
    std::unique_ptr<ProblemParameters>
    load_problem_parameters(const toml::table& problem) const;

    WeightedVector<SelectionParameters>
    load_selections_parameters(const GeneticAlgorithmParameters& ga_parameters,
                               const toml::array& selections) const;

    WeightedVector<CrossoverParameters>
    load_crossovers_parameters(const ProblemParameters& problem_parameters,
                               const toml::array& crossovers) const;

    WeightedVector<MutationParameters>
    load_mutations_parameters(const ProblemParameters& problem_parameters,
                              const toml::array& mutations) const;

    std::unique_ptr<PopulationInitializerParameters>
    load_initializer_parameters(const ProblemParameters& problem_parameters,
                                const GeneticAlgorithmParameters& ga_parameters,
                                const toml::table& initializer) const;

    std::unique_ptr<SSGAInspectorParameters>
    load_inspector_parameters(const toml::table& inspector) const;
};

template<typename ProblemParameters>
auto SSGAExperimentParametersLoaderTOML<ProblemParameters>::load(
    const toml::table& experiment) const -> SSGAExperimentParameters
{
    const auto algorithm_name = load_algorithm_name(experiment);

    auto problem_parameters =
        load_problem_parameters(*experiment["problem"].as_table());

    TracePoint trace =
        NamedTracePoint{"algorithm", std::string{algorithm_name}};
    ;

    try {
        const auto& algorithm = *experiment["algorithm"].as_table();

        const auto ga_parameters = load_genetic_algorithm_parameters(algorithm);

        if (!algorithm["selections"].is_array_of_tables()) {
            throw InvalidNodeError{{BasicTracePoint{"selections"}}};
        }
        WeightedVector<SelectionParameters> selections_parameters =
            load_selections_parameters(ga_parameters,
                                       *algorithm["selections"].as_array());

        if (!algorithm["crossovers"].is_array_of_tables()) {
            throw InvalidNodeError{{BasicTracePoint{"crossovers"}}};
        }
        WeightedVector<CrossoverParameters> crossovers_parameters =
            load_crossovers_parameters(*problem_parameters,
                                       *algorithm["crossovers"].as_array());

        if (!algorithm["mutations"].is_array_of_tables()) {
            throw InvalidNodeError{{BasicTracePoint{"mutations"}}};
        }
        WeightedVector<MutationParameters> mutations_parameters =
            load_mutations_parameters(*problem_parameters,
                                      *algorithm["mutations"].as_array());

        const auto* initializer_table_ptr = algorithm["initializer"].as_table();
        if (!initializer_table_ptr) {
            throw InvalidNodeError{{BasicTracePoint{"initializer"}}};
        }
        auto initializer_parameters = load_initializer_parameters(
            *problem_parameters, ga_parameters, *initializer_table_ptr);

        const auto* inspector_table_ptr = algorithm["inspector"].as_table();
        if (!inspector_table_ptr) {
            throw InvalidNodeError{{BasicTracePoint{"inspector"}}};
        }
        auto inspector_parameters =
            load_inspector_parameters(*inspector_table_ptr);

        trace = BasicTracePoint{"terminations"};
        const auto* terminations_table_ptr =
            experiment["terminations"].as_table();
        if (!terminations_table_ptr) { throw InvalidNodeError{}; }
        auto terminations_parameters =
            load_termination_criteria_parameters(*terminations_table_ptr);

        return SSGAExperimentParameters{std::move(problem_parameters),
                                        std::move(ga_parameters),
                                        std::move(selections_parameters),
                                        std::move(crossovers_parameters),
                                        std::move(mutations_parameters),
                                        std::move(initializer_parameters),
                                        std::move(terminations_parameters),
                                        std::move(inspector_parameters),
                                        load_seed(algorithm)};
    } catch (const TraceError& exc) {
        exc.push_trace_point(std::move(trace));
        throw;
    }
}

template<typename ProblemParameters>
auto SSGAExperimentParametersLoaderTOML<ProblemParameters>::
    load_problem_parameters(const toml::table& problem) const
    -> std::unique_ptr<ProblemParameters>
{
    std::optional<std::string> name;
    try {
        name = problem["name"].value<std::string>();
        if (!name) { throw InvalidNodeError{{BasicTracePoint{"name"}}}; }

        const auto it = problem_parameters_loaders.find(*name);
        if (it == problem_parameters_loaders.end()) {
            throw MissingLoaderError{
                problem_parameters_loaders, *name, "problem"};
        }
        const auto loader = it->second;
        return loader(problem);
    } catch (const TraceError& exc) {
        if (name) {
            exc.push_trace_point(NamedTracePoint{"problem", std::move(*name)});
        } else {
            exc.push_trace_point(BasicTracePoint{"problem"});
        }
        throw;
    }
}

template<typename ProblemParameters>
auto SSGAExperimentParametersLoaderTOML<ProblemParameters>::
    load_selections_parameters(const GeneticAlgorithmParameters& ga_parameters,
                               const toml::array& selections) const
    -> WeightedVector<SelectionParameters>
{
    std::size_t i = 0;
    std::optional<std::string> name;
    try {
        WeightedVector<SelectionParameters> res;
        res.reserve(selections.size());
        for (const auto& selection : selections) {
            name = std::nullopt;

            const auto* sel_table_ptr = selection.as_table();
            if (!sel_table_ptr) { throw InvalidNodeError{}; }
            const auto& sel_table = *sel_table_ptr;

            name = sel_table["name"].value<std::string>();
            if (!name) { throw InvalidNodeError{{BasicTracePoint{"name"}}}; }

            auto weight = sel_table["weight"].value<double>();
            if (!weight) {
                throw InvalidNodeError{{BasicTracePoint{"weight"}}};
            }

            const auto it = selection_parameters_loaders.find(*name);
            if (it == selection_parameters_loaders.end()) {
                throw MissingLoaderError{
                    selection_parameters_loaders, *name, "selection"};
            }
            const auto loader         = it->second;
            auto selection_parameters = loader(ga_parameters, sel_table);
            res.push_back(
                WeightedEntry{std::move(selection_parameters), *weight});
            ++i;
        }
        return res;
    } catch (const TraceError& exc) {
        if (name) {
            exc.push_trace_point(
                NamedIndexedTracePoint{"selections", i, std::move(*name)});
        } else {
            exc.push_trace_point(IndexedTracePoint{"selections", i});
        }
        throw;
    }
}

template<typename ProblemParameters>
auto SSGAExperimentParametersLoaderTOML<ProblemParameters>::
    load_crossovers_parameters(const ProblemParameters& problem_parameters,
                               const toml::array& crossovers) const
    -> WeightedVector<CrossoverParameters>
{
    std::size_t i = 0;
    std::optional<std::string> name;
    try {
        WeightedVector<CrossoverParameters> res;
        res.reserve(crossovers.size());
        for (const auto& crossover : crossovers) {
            name = std::nullopt;

            const auto* crx_table_ptr = crossover.as_table();
            if (!crx_table_ptr) { throw InvalidNodeError{}; }
            const auto& crx_table = *crx_table_ptr;

            name = crx_table["name"].value<std::string>();
            if (!name) { throw InvalidNodeError{{BasicTracePoint{"name"}}}; }

            auto weight = crx_table["weight"].value<double>();
            if (!weight) {
                throw InvalidNodeError{{BasicTracePoint{"weight"}}};
            }

            const auto it = crossover_parameters_loaders.find(*name);
            if (it == crossover_parameters_loaders.end()) {
                throw MissingLoaderError{
                    crossover_parameters_loaders, *name, "crossover"};
            }
            const auto loader         = it->second;
            auto crossover_parameters = loader(problem_parameters, crx_table);
            res.push_back(
                WeightedEntry{std::move(crossover_parameters), *weight});
            ++i;
        }
        return res;
    } catch (const TraceError& exc) {
        if (name) {
            exc.push_trace_point(
                NamedIndexedTracePoint{"crossovers", i, std::move(*name)});
        } else {
            exc.push_trace_point(IndexedTracePoint{"crossovers", i});
        }
        throw;
    }
}

template<typename ProblemParameters>
auto SSGAExperimentParametersLoaderTOML<ProblemParameters>::
    load_mutations_parameters(const ProblemParameters& problem_parameters,
                              const toml::array& mutations) const
    -> WeightedVector<MutationParameters>
{
    std::size_t i = 0;
    std::optional<std::string> name;
    try {
        WeightedVector<MutationParameters> res;
        res.reserve(mutations.size());
        for (const auto& mutation : mutations) {
            name = std::nullopt;

            const auto* mut_table_ptr = mutation.as_table();
            if (!mut_table_ptr) { throw InvalidNodeError{}; }
            const auto& mut_table = *mut_table_ptr;

            name = mut_table["name"].value<std::string>();
            if (!name) { throw InvalidNodeError{{BasicTracePoint{"name"}}}; }

            auto weight = mut_table["weight"].value<double>();
            if (!weight) {
                throw InvalidNodeError{{BasicTracePoint{"weight"}}};
            }

            const auto it = mutation_parameters_loaders.find(*name);
            if (it == mutation_parameters_loaders.end()) {
                throw MissingLoaderError{
                    mutation_parameters_loaders, *name, "mutation"};
            }
            const auto loader        = it->second;
            auto mutation_parameters = loader(problem_parameters, mut_table);
            res.push_back(
                WeightedEntry{std::move(mutation_parameters), *weight});
        }
        return res;
    } catch (const TraceError& exc) {
        if (name) {
            exc.push_trace_point(
                NamedIndexedTracePoint{"mutations", i, std::move(*name)});
        } else {
            exc.push_trace_point(IndexedTracePoint{"mutations", i});
        }
        throw;
    }
}

template<typename ProblemParameters>
auto SSGAExperimentParametersLoaderTOML<ProblemParameters>::
    load_initializer_parameters(const ProblemParameters& problem_parameters,
                                const GeneticAlgorithmParameters& ga_parameters,
                                const toml::table& initializer) const
    -> std::unique_ptr<PopulationInitializerParameters>
{
    std::optional<std::string> name;
    try {
        auto name = initializer["name"].value<std::string>();
        if (!name) { throw InvalidNodeError{{BasicTracePoint{"name"}}}; }

        const auto it = initializer_parameters_loaders.find(*name);
        if (it == initializer_parameters_loaders.end()) {
            throw MissingLoaderError{
                initializer_parameters_loaders, *name, "initializer"};
        }
        const auto loader = it->second;
        auto initializer_parameters =
            loader(ga_parameters.get_population_size(),
                   problem_parameters,
                   initializer);
        return initializer_parameters;
    } catch (const TraceError& exc) {
        if (name) {
            exc.push_trace_point(
                NamedTracePoint{"initializer", std::move(*name)});
        } else {
            exc.push_trace_point(BasicTracePoint{"initializer"});
        }
        throw;
    }
}

template<typename ProblemParameters>
auto SSGAExperimentParametersLoaderTOML<ProblemParameters>::
    load_inspector_parameters(const toml::table& inspector) const
    -> std::unique_ptr<SSGAInspectorParameters>
{
    std::optional<std::string> name;
    try {
        name = inspector["name"].value<std::string>();
        if (!name) { throw InvalidNodeError{{BasicTracePoint{"name"}}}; }

        const auto it = inspector_parameters_loaders.find(*name);
        if (it == inspector_parameters_loaders.end()) {
            throw MissingLoaderError{
                inspector_parameters_loaders, *name, "inspector"};
        }
        const auto& loader        = it->second;
        auto inspector_parameters = loader->load(inspector);
        return inspector_parameters;
    } catch (const TraceError& exc) {
        if (name) {
            exc.push_trace_point(
                NamedTracePoint{"inspector", std::move(*name)});
        } else {
            exc.push_trace_point(BasicTracePoint{"inspector"});
        }
        throw;
    }
}

}

#endif
