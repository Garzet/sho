#ifndef SHO_TUNE_PROBLEM_HPP
#define SHO_TUNE_PROBLEM_HPP

#include "tune/Solution.hpp"

#include <SHO/SO/Evaluator.hpp>
#include <SHO/MO/Evaluator.hpp>
#include <SHO/Solution.hpp>

#include <filesystem>

namespace tune {

namespace binary_vector {

class OneMaxProblem {
  public:
    class Evaluator : public SHO::SO::Evaluator<Solution> {
      public:
        double evaluate(const Solution& solution) override;
    };
};

}

namespace floating_point {

class SquareProblem {
  public:
    class Evaluator : public SHO::SO::Evaluator<Solution> {
      private:
        SHO::RealFunctionDomain domain;

      public:
        explicit Evaluator(SHO::RealFunctionDomain domain) noexcept;

        double evaluate(const Solution& solution) override;
    };
};

class SchafferProblem {
  public:
    using Fitness = std::array<double, 2>;

    static constexpr SHO::RealFunctionDomain domain = {-1000.0, 1000.0};

    class Evaluator : public SHO::MO::Evaluator<Solution, 2> {
      public:
        static constexpr SHO::MO::FitnessDomain<2> fitness_domain = {
            SHO::RealFunctionDomain{domain.min * domain.max, 0},
            SHO::RealFunctionDomain{domain.min * domain.max, 0}};

      public:
        Fitness evaluate(const Solution& solution) override;
    };
};

}

namespace permutation_vector {

class TravelingSalesmanProblem {
  public:
    class DistanceMatrix {
      private:
        using MatrixInitialierList =
            std::initializer_list<std::initializer_list<unsigned>>;

      private:
        std::size_t n_nodes;
        std::vector<unsigned> data;

      public:
        explicit DistanceMatrix(std::size_t n_nodes) noexcept;
        DistanceMatrix(MatrixInitialierList matrix) noexcept;

        void set_distance(std::size_t start,
                          std::size_t destination,
                          unsigned distance,
                          bool symmetric = true) noexcept;

        unsigned get_distance(std::size_t start,
                              std::size_t destination) const noexcept;

        std::size_t get_number_of_nodes() const noexcept;

        static DistanceMatrix
        load_from_tsplib_file(const std::filesystem::path& file);
    };

    class Evaluator : public SHO::SO::Evaluator<Solution> {
      private:
        DistanceMatrix distance_matrix;

      public:
        explicit Evaluator(DistanceMatrix distance_matrix) noexcept;
        double evaluate(const Solution& solution) override;
    };
};

}

}

#endif
