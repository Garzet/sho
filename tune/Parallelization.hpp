#ifndef SHO_TUNE_PARALLELIZATION_HPP
#define SHO_TUNE_PARALLELIZATION_HPP

#include "tune/ExperimentParameters.hpp"
#include "tune/ThreadPool.hpp"

#include <vector>
#include <future>

namespace tune::parallelization {

template<typename Solution>
using SSGAResult = typename SHO::SO::SSGA<Solution>::Result;

template<typename Solution>
using SSGAExperimentParameters =
    typename tune::SSGAExperimentParameters<Solution>;

template<typename Solution>
std::vector<std::future<SSGAResult<Solution>>> parallelize_ssga_experiments(
    const std::vector<SSGAExperimentParameters<Solution>>&
        ssga_experiments_parameters,
    unsigned n_runs,
    tune::ThreadPool& thread_pool)
{
    std::vector<std::future<SSGAResult<Solution>>> res;
    for (const auto& ssga_experiment_parameters : ssga_experiments_parameters) {
        for (unsigned run = 0; run < n_runs; ++run) {
            // The std::promise must be passed as std::shared_ptr to a task
            // lambda capture since std::function must be copyable.
            auto result_promise =
                std::make_shared<std::promise<SSGAResult<Solution>>>();
            res.push_back(result_promise->get_future());
            thread_pool.add_task(
                [&ssga_experiment_parameters,
                 result_promise = std::move(result_promise)]() {
                    result_promise->set_value(ssga_experiment_parameters.run());
                });
        }
    }
    return res;
}

}

#endif
