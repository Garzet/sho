#include "tune/ExperimentParameters.hpp"
#include "tune/AlgorithmParameters.hpp"
#include "tune/ProblemParameters.hpp"

namespace tune {

ExperimentParameters::ExperimentParameters(
    TerminationsParameters terminations_parameters) noexcept
        : terminations_parameters{std::move(terminations_parameters)}
{}

auto ExperimentParameters::get_terminations_parameters() const noexcept
    -> const TerminationsParameters&
{
    return terminations_parameters;
}

std::vector<std::unique_ptr<SHO::TerminationCriterion>>
ExperimentParameters::load_termination_criteria() const
{
    std::vector<std::unique_ptr<SHO::TerminationCriterion>> res;
    res.reserve(terminations_parameters.size());
    for (const auto& termination_parameters : terminations_parameters) {
        res.push_back(termination_parameters->load());
    }
    return res;
}

}
