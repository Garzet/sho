#ifndef SHO_SO_POPULATION_HPP
#define SHO_SO_POPULATION_HPP

#include "SHO/Assert.hpp"
#include "SHO/SO/Individual.hpp"

#include <vector>
#include <algorithm>

namespace SHO::SO {

template<typename Solution>
class Population : public std::vector<Individual<Solution>> {
  public:
    using Individual    = SHO::SO::Individual<Solution>;
    using ConstIterator = typename std::vector<Individual>::const_iterator;

  public:
    explicit Population(std::vector<Solution> solutions) noexcept;

    void sort();
    bool is_sorted() const;

    ConstIterator find_best_individual() const;

  private:
    static bool comparator(const Individual& lhs,
                           const Individual& rhs) noexcept;
};

template<typename Solution>
Population<Solution>::Population(std::vector<Solution> solutions) noexcept
{
    this->reserve(solutions.size());
    for (auto& solution : solutions) {
        this->emplace_back(std::move(solution));
    }
}

template<typename Solution>
void Population<Solution>::sort()
{
    // Sort population by fitness (the best solution is at the front of the
    // population).
    std::sort(this->begin(), this->end(), comparator);
}

template<typename Solution>
bool Population<Solution>::is_sorted() const
{
    return std::is_sorted(this->begin(), this->end(), comparator);
}

template<typename Solution>
auto Population<Solution>::find_best_individual() const -> ConstIterator
{
    // Look for minimum element since comparator is reversed.
    return std::min_element(this->begin(), this->end(), comparator);
}

template<typename Solution>
bool Population<Solution>::comparator(const Individual& lhs,
                                      const Individual& rhs) noexcept
{
    return lhs.get_fitness() > rhs.get_fitness(); // Reverse ordering.
}

}

#endif
