#ifndef SHO_CROSSOVER_HPP
#define SHO_CROSSOVER_HPP

namespace SHO {

template<typename Solution>
class Crossover {
  public:
    virtual ~Crossover() = default;

    virtual Solution mate(const Solution& first_parent,
                          const Solution& second_parent) = 0;
};

}

#endif
