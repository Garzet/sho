#ifndef SHO_FUNCTIONDOMAIN_HPP
#define SHO_FUNCTIONDOMAIN_HPP

#include <ostream>
#include <fmt/ostream.h>

namespace SHO {

struct RealFunctionDomain {
    const double min;
    const double max;

    constexpr bool contains(double element) const noexcept;
    constexpr bool contains(RealFunctionDomain other) const noexcept;

    constexpr double size() const noexcept;
    constexpr bool is_valid() const noexcept;
};

std::ostream& operator<<(std::ostream& os, const RealFunctionDomain& d);

constexpr bool RealFunctionDomain::contains(double element) const noexcept
{
    return element >= min && element <= max;
}

constexpr bool
RealFunctionDomain::contains(RealFunctionDomain other) const noexcept
{
    return min <= other.min && max >= other.min;
}

constexpr double RealFunctionDomain::size() const noexcept
{
    return max - min;
}

constexpr bool RealFunctionDomain::is_valid() const noexcept
{
    return min < max;
}

}

template<>
struct fmt::formatter<SHO::RealFunctionDomain> : ostream_formatter {};

#endif
