project('SHO', 'cpp',
        version: '0.1.0',
        default_options: ['cpp_std=c++17',
                          'warning_level=3',
                          'b_ndebug=if-release'])

sho_sources = files('SHO/FunctionDomain.cpp',
                    'SHO/Random.cpp',
                    'SHO/Solution.cpp',
                    'SHO/TerminationCriterion.cpp')

sho_headers = files('SHO/Assert.hpp',
                    'SHO/Advancement.hpp',
                    'SHO/Crossover.hpp',
                    'SHO/FunctionDomain.hpp',
                    'SHO/Mutation.hpp',
                    'SHO/NeighborhoodProvider.hpp',
                    'SHO/Random.hpp',
                    'SHO/Solution.hpp',
                    'SHO/State.hpp',
                    'SHO/TerminationCriterion.hpp',
                    'SHO/MO/Evaluator.hpp',
                    'SHO/MO/ExhaustiveSearch.hpp',
                    'SHO/MO/Fitness.hpp',
                    'SHO/MO/Individual.hpp',
                    'SHO/MO/NSGA2.hpp',
                    'SHO/MO/Population.hpp',
                    'SHO/SO/Evaluator.hpp',
                    'SHO/SO/ExhaustiveSearch.hpp',
                    'SHO/SO/Individual.hpp',
                    'SHO/SO/LocalSearch.hpp',
                    'SHO/SO/Population.hpp',
                    'SHO/SO/SSGA.hpp')

sho_include = include_directories('.')

fmt_dep  = dependency('fmt', version: '>=9.0.0')
sho_deps = [fmt_dep]

sho_lib = static_library('SHO', sho_sources,
                         extra_files: sho_headers,
                         include_directories: sho_include,
                         dependencies: sho_deps)

sho_dep = declare_dependency(include_directories: sho_include,
                             link_with: sho_lib,
                             dependencies: sho_deps)

if get_option('tune')
    subdir('tune')
endif

if get_option('examples')
    subdir('examples')
endif

if get_option('tests')
    subdir('test')
endif
