#ifndef SHO_TUNE_INITIALIZERPARAMETERS_HPP
#define SHO_TUNE_INITIALIZERPARAMETERS_HPP

#include "tune/Solution.hpp"

#include "SHO/FunctionDomain.hpp"
#include "SHO/Assert.hpp"

#include <vector>
#include <stdexcept>
#include <sstream>

namespace tune {

template<typename Solution>
class InitializerParameters {
  public:
    virtual ~InitializerParameters() = default;

    virtual Solution load() const = 0;
};

template<typename Solution>
class PopulationInitializerParameters {
  private:
    unsigned population_size;

  public:
    explicit PopulationInitializerParameters(unsigned population_size);
    virtual ~PopulationInitializerParameters() = default;

    virtual std::vector<Solution> load() const = 0;

    unsigned get_population_size() const noexcept;
};

namespace floating_point {

class RandomPopulationInitializerParameters final
        : public PopulationInitializerParameters<Solution> {
  private:
    SHO::RealFunctionDomain range;

  public:
    RandomPopulationInitializerParameters(unsigned population_size,
                                          SHO::RealFunctionDomain range,
                                          SHO::RealFunctionDomain domain);

    std::vector<Solution> load() const final;
};

class FixedPopulationInitializerParameters final
        : public PopulationInitializerParameters<Solution> {
  public:
    using PopulationInitializerParameters =
        tune::PopulationInitializerParameters<Solution>;

  private:
    std::vector<Solution> solutions;

  public:
    FixedPopulationInitializerParameters(std::vector<Solution> solutions,
                                         SHO::RealFunctionDomain domain);

    std::vector<Solution> load() const final;
};

}

namespace permutation_vector {

class RandomPopulationInitializerParameters final
        : public PopulationInitializerParameters<Solution> {
  private:
    std::size_t solution_size;

  public:
    RandomPopulationInitializerParameters(unsigned population_size,
                                          std::size_t solution_size);

    std::vector<Solution> load() const final;

    unsigned get_solution_size() const noexcept;
};

}

template<typename Solution>
PopulationInitializerParameters<Solution>::PopulationInitializerParameters(
    unsigned population_size)
        : population_size{population_size}
{
    // Population size is validated by the algorithm parameters object.
    SHO_ASSERT(population_size > 0, "Population size must be at least one.");
}

template<typename Solution>
unsigned
PopulationInitializerParameters<Solution>::get_population_size() const noexcept
{
    return population_size;
}

}

#endif
